# vue3-minimal

Ce projet permet de construire rapidement un site avec VueJS 3 avec le minimum de composants installés

## Recommandation pour le développement

Installez les extensions proposées quand vous lancez VSCode.

## Pour modifier la configuration de Vite

Voir [Vite Configuration Reference](https://vitejs.dev/config/).

## Installation des dépendances

```sh
npm install
```

### Routage

- Le routage est déjà installé avec une configuration minimale présent dans le dossier src/router

### Gestion de l'état avec Pinia

- Un store minimal est déjà installé présent dans le dossier src/stores

### Lancer le serveur de développement

```sh
npm run dev
```

### Avoir un rendu pour la production

```sh
npm run build
```
