import { createRouter, createWebHistory } from 'vue-router'
import ARenommerVue from "../components/ARenommer.vue"

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    // À compléter
    {
      path: '/',
      name : 'homepage',
      component : ARenommerVue
    }
  ]
})

export default router
